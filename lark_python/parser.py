"""
Source: https://github.com/lark-parser/lark/blob/master/examples/advanced/python_parser.py
Grammar-complete Python Parser
==============================

A fully-working Python 2 & 3 parser (but not production ready yet!)

This example demonstrates usage of the included Python grammars
"""
from lark import Lark
from lark.indenter import Indenter
from pathlib import Path

# __path__ = os.path.dirname(__file__)


class PythonIndenter(Indenter):
    NL_type = '_NEWLINE'
    OPEN_PAREN_types = ['LPAR', 'LSQB', 'LBRACE']
    CLOSE_PAREN_types = ['RPAR', 'RSQB', 'RBRACE']
    INDENT_type = '_INDENT'
    DEDENT_type = '_DEDENT'
    tab_len = 8


kwargs = dict(rel_to=__file__, postlex=PythonIndenter(), start='file_input')


def Python3Parser(**user_kwargs):
    grammar_path = Path(__file__).parent/"python3.lark"
    real_kwargs = dict(kwargs)
    for k, v in user_kwargs.items():
        real_kwargs[k] = v
    return Lark.open(grammar_path, parser='lalr', **real_kwargs)
